CFLAGS+=-c -Wall -std=c11 -g -fPIC -D_DEFAULT_SOURCE -mno-tbm
INCLUDES+=-I../libbencode -I../libnntorrent
LDFLAGS+=-L../libbencode -L../libnntorrent -lbencode -lnntorrent -Wl,-rpath,../libbencode -Wl,-rpath,../libnntorrent -lcrypto -lm -lgmp -pthread -lcurl 

SRCS=$(wildcard *.c)
#OBJS=$(addsuffix .o, $(basename $(SRCS)))
OBJS=main.o session.o
EXECUTABLE=nntorrent

ifeq ($(shell uname -s),FreeBSD)
	CC=clang
	INCLUDES+=-I/usr/local/include
	LDFLAGS+=-L/usr/local/lib
else
	CC=gcc
endif

.SUFFIXES: .c

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	$(CC) $(LDFLAGS) $^ -o $@

.c.o: $(SRCS)
	$(CC) $(CFLAGS) $(INCLUDES) $< -o $@

clean:
	rm -rf $(OBJS) $(EXECUTABLE) $(LIBNAME)
