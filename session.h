#ifndef SESSION_H
#define SESSION_H

#include <pthread.h>

#include "torrent.h"
#include "evqueue.h"

#define MAX_CLIENTS 10

typedef struct {
  torrent_t *torrent;
  peer_t *peer;
} conn_t;

typedef struct {
  // torrent_t is a linked list
  torrent_t *first_torrent;
  torrent_t *last_torrent;

  pthread_t *thread_list;
  size_t thread_num;
  size_t thread_max;

  conn_t **conns;
  int conns_len;
  int conns_max;

  /* all the file descriptors we're gonna deal with */
  int loop_fd;
  int cleanup_timer_id;
  int server_fd;
  int cmd_fd;
  int notify_fd;

  /* options */
  char *download_dir;
  char *ctl_socket_path;

  int clients[MAX_CLIENTS];

  bool quit;
} session_t;

session_t *nn_session_new();
void nn_session_free(session_t *);

torrent_t *nn_session_add_torrent(session_t *, const char *filename);
int nn_session_del_torrent(session_t *, const char *id);
conn_t *_nn_session_add_conn(session_t *, torrent_t *, peer_t *);
void _nn_session_del_conn(session_t *, conn_t *c);
int nn_session_mainloop(session_t *);

int nn_session_add_client(session_t *, int);
int nn_session_del_client(session_t *, int);
bool nn_session_is_client(session_t *, int);
int nn_session_broadcast_message(session_t *, const char *);
int nn_session_broadcast_event(session_t *, struct evqueue_event *e);
int nn_session_do_cmd(session_t *, char *, size_t);

int _setup_timers(session_t *);
int _setup_server_socket(session_t *);

int nn_session_set_download_dir(session_t *, char *);
int nn_session_set_socket_path(session_t *, char *);

#endif // SESSION_H
