#include <signal.h>
#include <getopt.h>
#include <syslog.h>

#include "session.h"

void
usage()
{
  printf("usage: nntorrent [--download-dir=<path>] [--socket=<path>]\n");
  printf("\n");
  printf("--download-dir    directory where downloaded files will be\n");
  printf("--socket          filename of the control socket\n");
}

int
main(int argc, char **argv)
{
  static struct option longopts[] = {
    { "download-dir",   required_argument, NULL, 'd' },
    { "socket",         required_argument, NULL, 's'},
    { "help",           no_argument,       NULL, 'h'},
    { NULL,             0,                 NULL, 0 }
  };

  int ch;
  char *download_dir = NULL;
  char *socket_fname = NULL;

  while ((ch = getopt_long(argc, argv, "d:s:", longopts, NULL)) != -1) {
    switch(ch) {
      case 'd':
        download_dir = malloc(strlen(optarg) + 1);
        strcpy(download_dir, optarg);
        break;

      case 's':
        socket_fname = malloc(strlen(optarg) + 1);
        strcpy(socket_fname, optarg);
        break;

      case 'h':
        usage();
        exit(EXIT_SUCCESS);

      default:
        usage();
    }
  }

  if (!(download_dir && socket_fname)) {
    usage();
    return EXIT_FAILURE;
  }


  /*setlogmask(LOG_DEBUG);*/
  openlog("nntorrent", LOG_PERROR, LOG_USER);

  syslog(LOG_INFO, "nntorrent starting...");

  session_t *session = nn_session_new();
  nn_session_set_download_dir(session, download_dir);
  nn_session_set_socket_path(session, socket_fname);

  syslog(LOG_INFO, "download directory = %s", session->download_dir);
  syslog(LOG_INFO, "control socket = %s", session->ctl_socket_path);

  free(download_dir);
  free(socket_fname);

  if (!session) {
    syslog(LOG_EMERG, "could not create session");
    closelog();
    return EXIT_FAILURE;
  }

  curl_global_init(CURL_GLOBAL_ALL);
  nn_session_mainloop(session);
  nn_session_free(session);

  closelog();
  curl_global_cleanup();

  fclose(stdin);
  fclose(stdout);
  fclose(stderr);

  return EXIT_SUCCESS;
}

