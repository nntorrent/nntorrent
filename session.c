#include "session.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <stdlib.h>
#include <syslog.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "mainloop.h"
#include "tracker.h"
#include "bitfield.h"
#include "evqueue.h"

unsigned char *_get_message(int sock, uint32_t *len);
int _setup_cmd_socket(session_t *);

session_t *
nn_session_new()
{
  evqueue_init();

  session_t *s = malloc(sizeof(session_t));
  s->quit = false;

  s->first_torrent = NULL;
  s->last_torrent = NULL;

  s->thread_num = 0;
  s->thread_max = 1;
  s->thread_list = malloc(s->thread_max * sizeof(pthread_t));;

  s->conns_max = 16;
  s->conns_len = 0;
  s->conns = malloc(sizeof(conn_t *) * s->conns_max);
  memset(s->conns, 0, sizeof(conn_t *) * s->conns_max);

  s->loop_fd = ml_create();
  s->notify_fd = evqueue_fd();
  s->cleanup_timer_id = 0;

  for (int i = 0; i < MAX_CLIENTS; i++) {
    s->clients[i] = -1;
  }

  return s;
}

int
nn_session_set_download_dir(session_t *s, char *d)
{
  if (!(s->download_dir = realpath(d, NULL))) {
    syslog(LOG_DEBUG, "could not set download_dir: %m");
    return -1;
  }
  return 0;
}

int
nn_session_set_socket_path(session_t *s, char *d)
{
  if (!(s->ctl_socket_path = realpath(d, NULL))) {
    syslog(LOG_DEBUG, "could not set ctl_socket_path: %m");
    return -1;
  }
  return 0;
}

void
nn_session_free(session_t *s)
{
  if (!s)
    return;

  torrent_t *t = s->first_torrent;
  while (t) {
    torrent_t *t_next = t->next;
    nn_torrent_free(t);
    t = t_next;
  }

  if (s->conns) {
    for (int i = 0; i < s->conns_max; i++) {
      if (s->conns[i]) {
        free(s->conns[i]);
      }
    }
    free(s->conns);
  }

  if (s->thread_list) {
    free(s->thread_list);
  }

  if (s->download_dir) {
    free(s->download_dir);
  }

  if (s->ctl_socket_path) {
    free(s->ctl_socket_path);
  }

  close(s->cleanup_timer_id);
  close(s->loop_fd);
  close(s->server_fd);
  close(s->cmd_fd);
  close(s->notify_fd);

  for (int i = 0; i < MAX_CLIENTS; i++) {
    if (s->clients[i] > 0) {
      close(s->clients[i]);
    }
  }

  evqueue_destroy();

  free(s);
}

torrent_t *
nn_session_add_torrent(session_t *s, const char *filename)
{
  char *fn = realpath(filename, NULL);

  if (!fn) {
    syslog(LOG_ERR, "could not open %s: %m", filename);
    return NULL;
  }

  torrent_t *temp = nn_torrent_new(fn, s->download_dir);
  free(fn);

  if (!temp) {
    return NULL;
  }

  if (!s->first_torrent)
    s->first_torrent = temp;
  else
    s->last_torrent->next = temp;

  s->last_torrent = temp;

  if (s->thread_num >= s->thread_max) {
    s->thread_max += 2;
    s->thread_list = realloc(s->thread_list, s->thread_max * sizeof(pthread_t));
  }

  pthread_create(&s->thread_list[s->thread_num++], NULL, nn_torrent_run, (void *) temp);

  return temp;
}

int
nn_session_del_torrent(session_t *s, const char *id)
{
  torrent_t *t = s->first_torrent;

  if (memcmp(t->id, id, TORRENT_ID_SIZE) == 0) {
    s->first_torrent = t->next;
    nn_torrent_quit(t);
    return 0;
  }

  while (t->next) {
    if (memcmp(t->next->id, id, TORRENT_ID_SIZE) == 0) {
      torrent_t *temp = t->next;
      t->next = temp->next;
      nn_torrent_quit(temp);
      return 0;
    }

    t = t->next;
  }

  return -1;
}

int
_setup_cmd_socket(session_t *s)
{
  /*
   * setup the command socket so we can listen for clients
   */
  struct sockaddr_un sa;
  sa.sun_family = AF_UNIX;

  if (s->ctl_socket_path) {
    strcpy(sa.sun_path, s->ctl_socket_path);
  } else {
    strcpy(sa.sun_path, "nnt.sock");
  }

  int len = sizeof(sa.sun_family) + strlen(sa.sun_path) + 1;

  unlink(sa.sun_path);

  if ((s->cmd_fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    syslog(LOG_DEBUG, "could not create command socket: %m");
    return -1;
  }

  if (bind(s->cmd_fd, (struct sockaddr *) &sa, len) != 0) {
    syslog(LOG_DEBUG, "could not bind command socket: %m");
    return -1;
  }

  if (listen(s->cmd_fd, 10) != 0) {
    syslog(LOG_DEBUG, "could not listen to server socket: %m");
    return -1;
  }

  ml_add_fd(s->loop_fd, s->cmd_fd, ML_READ, NULL);

  return 0;
}

int
_setup_timers(session_t *s)
{
  /*
   * set up a clean up timer
   */
  s->cleanup_timer_id = ml_create_timer(s->loop_fd, 1);
  if (s->cleanup_timer_id == -1) {
    syslog(LOG_DEBUG, "could not create a timer: %m");
    nn_session_free(s);
    return -1;
  }

  /*
   * set a timer so we can take care of chores once in a while
   */
  ml_set_timer(s->loop_fd, s->cleanup_timer_id, 2, false);

  return 0;
}

int
_setup_server_socket(session_t *s)
{
  /*
   * setup the server socket so we can listen for the poor souls
   * who want to download from us
   */
  struct addrinfo hints;
  struct addrinfo *res;
  memset(&hints, 0, sizeof(struct addrinfo));

  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;

  int ret;
  if ((ret = getaddrinfo("0.0.0.0", "9000", &hints, &res)) != 0) {
    syslog(LOG_DEBUG, "could not get server address info: %s", gai_strerror(ret));
    return -1;
  }

  if ((s->server_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) == -1) {
    syslog(LOG_DEBUG, "could not create server socket: %m");
    freeaddrinfo(res);
    return -1;
  }

  int optval = 1;
  if (setsockopt(s->server_fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int)) != 0) {
    syslog(LOG_DEBUG, "could not set SO_REUSEADDR on server socket: %m");
  }

  if (bind(s->server_fd, res->ai_addr, res->ai_addrlen) != 0) {
    syslog(LOG_DEBUG, "could not bind server socket: %m");
    freeaddrinfo(res);
    return -1;
  }

  if (listen(s->server_fd, 10) != 0) {
    syslog(LOG_DEBUG, "could not listen to server socket: %m");
    freeaddrinfo(res);
    return -1;
  }

  freeaddrinfo(res);

  ml_add_fd(s->loop_fd, s->server_fd, ML_READ, NULL);

  return 0;
}

int
nn_session_mainloop(session_t *s)
{
  if (_setup_server_socket(s) != 0) {
    syslog(LOG_ERR, "could not setup server socket");
    return -1;
  }

  _setup_cmd_socket(s);

  if (ml_add_fd(s->loop_fd, s->notify_fd, ML_READ, NULL) == -1) {
    syslog(LOG_WARNING, "could not initialize notifications");
  }

  while (!s->quit) {
    ml_event_t eevents[10];
    int num_events = ml_get_events(s->loop_fd, eevents, 10);

    if (num_events < 0) {
      if (errno == EINTR) {
        continue;
      }
      syslog(LOG_EMERG, "mainloop received irrecoverable signal: %m");
      return -1;
    }

    else {
      for (int i = 0; i < num_events; i++) {
        /*
         * any interested peers out there?
         */
        if (ml_event_read(&eevents[i]) && ml_event_fd(&eevents[i]) == s->server_fd) {
          int peer_fd;
          struct sockaddr_in addr;
          socklen_t len = sizeof(addr);

          if ((peer_fd = accept(s->server_fd, (struct sockaddr *) &addr, &len)) == -1) {
            syslog(LOG_DEBUG, "could not accept connection from peer: %m");
            continue;
          }

          peer_t *p = peer_new_incoming(peer_fd, NULL, NULL);
          p->ip = addr.sin_addr.s_addr;
          p->port = ntohs(addr.sin_port);

          struct in_addr in;
          in.s_addr = p->ip;
          syslog(LOG_DEBUG, "incoming peer %s:%d connected", inet_ntoa(in), p->port);

          if (peer_receive_handshake(p) != 0) {
            syslog(LOG_DEBUG, "session: handshake verification failed");
            peer_free(p);
          }

          /* which torrent does the peer want? */
          for (torrent_t *t = s->first_torrent; t; t = t->next) {
            if (memcmp(p->info_hash, t->metainfo->info_hash, 20) == 0) {
              /* FOUND IT */
              if (nn_torrent_add_incoming_peer(t, p) != 0) {
                syslog(LOG_DEBUG, "could not add incoming peer %s:%d", inet_ntoa(in), p->port);
              }
              break;
            }
          }

        }

        /*
         * a client has connected
         */
        else if (ml_event_read(&eevents[i]) && ml_event_fd(&eevents[i]) == s->cmd_fd) {
          int fd = accept(s->cmd_fd, NULL, NULL);
          if (fd <= 0) {
            syslog(LOG_ERR, "could not accept connection from control socket: %m");
            continue;
          }

          nn_session_add_client(s, fd);
          ml_add_fd(s->loop_fd, fd, ML_READ, NULL);
        }

        /*
         * a client has sent us a command
         */
        else if (nn_session_is_client(s, ml_event_fd(&eevents[i]))) {
          char buf[1024];
          memset(buf, 0, 1024);
          int ret = recv(ml_event_fd(&eevents[i]), buf, 1023, 0);

          if (ret == 0) {
            nn_session_del_client(s, ml_event_fd(&eevents[i]));
          }

          /* strip the \n */
          char *newline = strchr(buf, '\n');
          if (newline) {
            *newline = '\0';
          }

          nn_session_do_cmd(s, buf, 1024);
        }

        /*
         * notification?
         */
        else if (ml_event_read(&eevents[i]) && ml_event_fd(&eevents[i]) == s->notify_fd) {
          /* process all events */
          for (struct evqueue_event *e = evqueue_dequeue(); e; e = evqueue_dequeue()) {
            nn_session_broadcast_event(s, e);
            evqueue_event_free(e);
          }
        }

        /*
         * this shouldn't really happen
         */
        else {
          syslog(LOG_DEBUG, "unknown mainloop event occurred");
          continue;
        }

      }
    }
  }
  
  for (torrent_t *t = s->first_torrent; t; t = t->next) {
    syslog(LOG_DEBUG, "closing torrent %s", t->metainfo->info->name);
    nn_torrent_quit(t);
  }

  for (int i = 0; i < s->thread_num; i++) {
    if (pthread_join(s->thread_list[i], NULL) != 0) {
      syslog(LOG_ERR, "could not gracefully shutdown torrent index %d", i);
      continue;
    }
  }

  syslog(LOG_DEBUG, "shutting down nntorrent");

  return 0;
}

int
nn_session_add_client(session_t *s, int fd)
{
  for (int i = 0; i < MAX_CLIENTS; i++) {
    if (s->clients[i] <= 0) {
      s->clients[i] = fd;
      return 0;
    }
  }

  return -1;
}

int
nn_session_del_client(session_t *s, int fd)
{
  close(fd);

  for (int i = 0; i < MAX_CLIENTS; i++) {
    if (s->clients[i] == fd) {
      s->clients[i] = -1;
      return 0;
    }
  }

  return -1;
}

bool
nn_session_is_client(session_t *s, int fd)
{
  for (int i = 0; i < MAX_CLIENTS; i++) {
    if (s->clients[i] == fd) {
      return true;
    }
  }

  return false;
}

int
nn_session_do_cmd(session_t *s, char *msg, size_t len)
{
  if (len < 1) {
    return -1;
  }

  if (strncmp(msg, "add", 3) == 0) {
    char *sep = strchr(msg, ' ');
    if (!sep) return -1;
    sep++; // get past the space

    torrent_t *t = nn_session_add_torrent(s, sep);
    if (!t) {
      return -1;
    }

    evqueue_enqueue(evqueue_event_new(t->id, EVQUEUE_EVENT_STARTED));
  }

  else if (strncmp(msg, "status", 6) == 0) {
    char *sep = strchr(msg, ' ');
    if (!sep) return -1;
    sep++; // get past the space

    for (torrent_t *t = s->first_torrent; t; t = t->next) {
      if (strncmp(sep, t->id, TORRENT_ID_SIZE) == 0) {
        /* status = status:<id>:<paused|running|completed>:<dir>:<name>:<multi|single>:<length>:<downloaded>:<uploaded>:<left> */
        int len = 1024;
        char res[len];
        memset(res, 0, len);

        char id[TORRENT_ID_SIZE + 1];
        memcpy(id, t->id, TORRENT_ID_SIZE);
        id[TORRENT_ID_SIZE] = '\0';

        snprintf(res, len, "%s:%s:%s:%s:%s:%s:%ld:%ld:%ld:%ld:%d\n",
                "status",
                id,
                t->done ? "completed" : "running",
                s->download_dir,
                t->metainfo->info->name,
                t->destfiles_num > 1 ? "multi" : "single",
                t->total_length,
                t->tr->req->downloaded,
                t->tr->req->uploaded,
                t->tr->req->left,
                t->peers_num
                );
        nn_session_broadcast_message(s, res);

        return 0;
      }
    }
  }

  else if (strncmp(msg, "list", 4) == 0) {
    char id[TORRENT_ID_SIZE + 1];
    char res[1024];

    for (torrent_t *t = s->first_torrent; t; t = t->next) {
      memcpy(id, t->id, TORRENT_ID_SIZE);
      id[TORRENT_ID_SIZE] = '\0';

      sprintf(res, "%s:%s\n", id, t->metainfo->info->name);
      nn_session_broadcast_message(s, res);
    }
  }

  else if (strncmp(msg, "quit", 4) == 0) {
    s->quit = true;
  }

  return 0;
}

int
nn_session_broadcast_message(session_t *s, const char *msg)
{
  for (int i = 0; i < MAX_CLIENTS; i++) {
    send(s->clients[i], msg, strlen(msg), MSG_NOSIGNAL);
  }

  return 0;
}

int
nn_session_broadcast_event(session_t *s, struct evqueue_event *e)
{
  char *type = evqueue_type_to_str(e->type);

  char id[TORRENT_ID_SIZE + 1];
  memcpy(id, e->torrent_id, TORRENT_ID_SIZE);
  id[TORRENT_ID_SIZE] = '\0';

  int len = 1024;
  char msg[len];
  memset(msg, 0, len);

  torrent_t *t;
  for (t = s->first_torrent; t; t = t->next) {
    if (strncmp(t->id, e->torrent_id, TORRENT_ID_SIZE) == 0) {
      break;
    }
  }

  if (!t) {
    syslog(LOG_DEBUG, "could not find torrent id associated with event");
    free(type);
    return -1;
  }

  sprintf(msg, "%s:%s:%s:%s\n", type, id, s->download_dir, t->metainfo->info->name);
  free(type);

  return nn_session_broadcast_message(s, msg);
}

conn_t *
_nn_session_add_conn(session_t *s, torrent_t *t, peer_t *p)
{
  if (s->conns_len == s->conns_max) {
    int old_max = s->conns_max;
    s->conns_max *= 2;
    s->conns = realloc(s->conns, sizeof(conn_t *) * s->conns_max);
    memset(s->conns + sizeof(conn_t *) * old_max, 0, (s->conns_max - old_max) * sizeof(conn_t *));
  }

  conn_t *c = malloc(sizeof(conn_t));
  c->torrent = t;
  c->peer = p;

  for (int i = 0; i < s->conns_max; i++) {
    if (!s->conns[i]) {
      s->conns[i] = c;
    }
  }

  s->conns_len++;

  return c;
}

void
_nn_session_del_conn(session_t *s, conn_t *c)
{
  ml_del_fd(s->loop_fd, c->peer->sock, ML_READ);
  nn_torrent_shutdown_peer(c->torrent, c->peer);
  free(c);
}

